import { ModuleWithProviders } from '@angular/core';
import { ServidorConfig } from './interfaces/servidor-config';
export declare class WebrunModule {
    static forRoot(config: ServidorConfig): ModuleWithProviders;
}

import { InjectionToken } from '@angular/core';
import { ServidorConfig } from '../interfaces/servidor-config';
export declare const ServidorConfigService: InjectionToken<ServidorConfig>;

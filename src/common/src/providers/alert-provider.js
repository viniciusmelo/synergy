import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { App } from 'ionic-angular/components/app/app';
import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
var AlertProvider = /** @class */ (function () {
    function AlertProvider(storage, menuCtrl, loadingCtrl, toastCtrl, alertCtrl, app) {
        this.storage = storage;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.app = app;
    }
    AlertProvider.prototype.showToast = function (message, duration) {
        if (duration === void 0) { duration = 2000; }
        this.toastCtrl.create({ message: message, duration: duration }).present();
    };
    AlertProvider.prototype.showAlert = function (title, message, btns, timeout, inputs) {
        var alert = this.alertCtrl.create({
            inputs: inputs,
            title: title,
            message: message,
            buttons: btns
        });
        alert.present();
        if (timeout) {
            setTimeout(function () {
                alert.dismiss();
            }, timeout);
        }
        return alert;
    };
    AlertProvider.prototype.showLoading = function (message) {
        return this.loadingCtrl.create({ content: message });
    };
    AlertProvider.prototype.showAlertExit = function (paginaRoot) {
        var _this = this;
        var alerta = this.alertCtrl.create({
            subTitle: 'Você tem certeza que deseja sair?',
            buttons: [
                {
                    text: 'Não',
                    handler: function () { }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.navCtrl.setRoot(paginaRoot);
                        _this.menuCtrl.enable(false);
                        _this.storage.remove('user');
                    }
                }
            ]
        });
        alerta.present();
    };
    Object.defineProperty(AlertProvider.prototype, "navCtrl", {
        get: function () {
            return this.app.getRootNav();
        },
        enumerable: true,
        configurable: true
    });
    AlertProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AlertProvider.ctorParameters = function () { return [
        { type: Storage },
        { type: MenuController },
        { type: LoadingController },
        { type: ToastController },
        { type: AlertController },
        { type: App }
    ]; };
    return AlertProvider;
}());
export { AlertProvider };
//# sourceMappingURL=alert-provider.js.map
import { Storage } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular/platform/platform';
import { MenuController, Events } from 'ionic-angular';
var GeralProvider = /** @class */ (function () {
    function GeralProvider(events, menuCtrl, storage, camera, platform, file) {
        this.events = events;
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.globalVar = {};
    }
    GeralProvider.prototype.removeCache = function () {
        var _this = this;
        if (this.platform.is('android')) {
            this.file.listDir(this.file.externalCacheDirectory, '').then(function (fotos) {
                if (fotos) {
                    if (fotos.length > 5) {
                        fotos.forEach(function (item) {
                            _this.file.removeFile(_this.file.externalCacheDirectory, item.name)
                                .then()
                                .catch(function (e) { return console.log(e); });
                            console.log('Photos removed from cache');
                        });
                    }
                }
            }).catch(function (e) { return console.log(e); });
        }
        else {
            this.camera.cleanup()
                .then(function () { return console.log('Photos removed from cache'); })
                .catch(function (e) { return console.error(e); });
        }
    };
    GeralProvider.prototype.initMenu = function (user, pages) {
        var _this = this;
        this.menuCtrl.enable(true);
        var menu = { 'pages': pages };
        if (user) {
            this.storage.set('user', JSON.stringify(user));
            menu['user'] = user;
            this.events.publish('menu:populate', menu);
        }
        else {
            this.storage.get('user').then(function (data) {
                user = JSON.parse(data);
                menu['user'] = user;
                _this.events.publish('menu:populate', menu);
            });
        }
    };
    GeralProvider.prototype.dateISOToLocalFormat = function (dateISO, time) {
        if (time === void 0) { time = false; }
        var fullDateParts = dateISO.split('T');
        var dateParts = fullDateParts[0].split('-');
        var timeParts = fullDateParts[1].split(':');
        var dateResult = dateParts[2] + "/" + dateParts[1] + "/" + dateParts[0];
        dateResult += (time) ? " " + timeParts[0] + ":" + timeParts[1] : '';
        return dateResult;
    };
    GeralProvider.prototype.dateLocalFormatToISO = function (dateLocalFormat, time) {
        if (time === void 0) { time = false; }
        var myDate = new Date(dateLocalFormat.substr(3, 2) + "/" + dateLocalFormat.substr(0, 2) + "/" + dateLocalFormat.substr(6, 4));
        if (time) {
            var fullDateParts = dateLocalFormat.split(' ');
            var timeParts = fullDateParts[1].split(':');
            myDate.setHours(+timeParts[0]);
            myDate.setMinutes(+timeParts[1]);
        }
        return myDate.toISOString();
    };
    GeralProvider.prototype.numeroAleatorio = function (mínimo, máximo) {
        if (mínimo === void 0) { mínimo = 0; }
        if (máximo === void 0) { máximo = 99999999; }
        return Math.round(Math.random() * (máximo - mínimo) + mínimo);
    };
    GeralProvider.prototype.calcularIdade = function (dataAniversario) {
        var ageDifMs = Date.now() - dataAniversario.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    };
    GeralProvider.prototype._workaroundFormControlApplyMask = function (element) {
        element.nativeElement.firstElementChild.dispatchEvent(new Event('focus'));
        element.nativeElement.firstElementChild.dispatchEvent(new Event('blur'));
        element.nativeElement.firstElementChild.dispatchEvent(new Event('input'));
    };
    GeralProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    GeralProvider.ctorParameters = function () { return [
        { type: Events },
        { type: MenuController },
        { type: Storage },
        { type: Camera },
        { type: Platform },
        { type: File }
    ]; };
    return GeralProvider;
}());
export { GeralProvider };
//# sourceMappingURL=geral-provider.js.map
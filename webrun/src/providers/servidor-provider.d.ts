import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Config } from '../models/config';
import { User } from 'common/src/models/user.model';
export declare class ServidorProvider {
    private file;
    private platform;
    private network;
    private transfer;
    private device;
    http: HttpClient;
    private storage;
    static CONFIG_NAME: string;
    private config;
    fileTransfer: FileTransferObject;
    private subject;
    private userLogged;
    private parametrosPadrao;
    constructor(file: File, platform: Platform, network: Network, transfer: FileTransfer, device: Device, http: HttpClient, storage: Storage);
    readonly user: User;
    init(): void;
    login(login: string, senha: string, modulo: string): Promise<any>;
    /**
     *
     * @param modulo
     * @param msg
     * @param acao
     * @param params
     * @param map - indica se o conteudo retornado é um array json em formato string, que precisa de parsing em cada item
     * @param parse - indice que o objeto retornado é um json e deve ter um parse
     */
    getRequest(modulo: string, msg: string, acao: string, params: any, map?: boolean, parse?: boolean): Observable<any>;
    obterDadosPadrao(modulo: string, buscarServidor?: boolean): Promise<any>;
    atualizarDadosPadrao(modulos: string[]): Promise<any>;
    atualizarDadoPadrao(modulo: string): Promise<any>;
    gravarDadoPadrao(modulo: string, dados: any): Promise<any>;
    obterDadoPadrao(modulo: string, dado: string): Promise<any>;
    definirVariaveis(config: Config): Config;
    downloadPhoto(caminhoFoto: string): Promise<any>;
    getProgressTransfer(): Observable<any>;
    uploadPhoto(foto: string, modulo: string, acao: string, params?: {}): Promise<any>;
    private requestDadosPadrao(modulo, data?);
    private obterDadosDispositivo(login);
    /**
     * Definir variável que guarda os parâmetros que toda requisição efetuada pelo método getRequest() irá mandar por padrão
     * @param parametros
     */
    definirParametrosPadrao(parametros: any): void;
    getConfig(init?: boolean): any;
}

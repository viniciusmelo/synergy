declare class Config {
    server: Server;
    url: string;
    port: string;
    sys: string;
    webrun: string;
    regraProcessar: string;
    port_ocr?: string;
    sys_second?: string;
}
declare class Server {
    urlBase: string;
    callWebRulePath: string;
    loginPath: string;
    urlCallRule: string;
    pathProcessar: string;
    urlOCR?: string;
    urlCallRuleSecondServer?: string;
    pathProcessarSecondServer?: string;
}
export { Config, Server };

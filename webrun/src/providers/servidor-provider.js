//import { User } from '../models/user.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { Md5 } from 'ts-md5';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { isArray } from 'ionic-angular/util/util';
import { Storage } from '@ionic/storage';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import * as _ from 'lodash';
import { Config } from '../models/config';
var ServidorProvider = /** @class */ (function () {
    function ServidorProvider(file, platform, network, transfer, device, http, storage) {
        var _this = this;
        this.file = file;
        this.platform = platform;
        this.network = network;
        this.transfer = transfer;
        this.device = device;
        this.http = http;
        this.storage = storage;
        this.fileTransfer = this.transfer.create();
        this.subject = new BehaviorSubject(0);
        this.init();
        if (!this.userLogged) {
            this.storage.get('user')
                .then(function (data) {
                if (data) {
                    _this.userLogged = JSON.parse(data);
                }
            });
        }
    }
    Object.defineProperty(ServidorProvider.prototype, "user", {
        get: function () {
            return this.userLogged;
        },
        enumerable: true,
        configurable: true
    });
    ServidorProvider.prototype.init = function () {
        this.getConfig(true);
    };
    ServidorProvider.prototype.login = function (login, senha, modulo) {
        var _this = this;
        //const loading = this.alertProvider.showLoading('Carregando...');
        //loading.present();
        this.init();
        var params = {
            "user": login + ""
        }, options = {
            method: "get",
            url: this.config.server['pathProcessar'],
            params: {
                params: "acao=pre_login|params=" + JSON.stringify(params) + "|acesso=" + JSON.stringify(this.obterDadosDispositivo(login)) + "|modulo=" + modulo
            }
        };
        return new Promise(function (resolve, reject) {
            //Pre login
            _this.http.request("GET", options.url, options)
                .timeout(120000)
                .toPromise()
                .then(function (data) {
                var resData = data;
                var res = JSON.parse(resData[0].value);
                if (res.success == 'false') {
                    //loading.dismiss();
                    reject(res.message);
                }
                if (res.success == 'true') {
                    var loginParams = {
                        "pass": Md5.hashStr(res.user + senha),
                        "user": params.user
                    };
                    //Login 
                    options.params.params = "acao=login|params=" + JSON.stringify(loginParams) + "|acesso=" + JSON.stringify(_this.obterDadosDispositivo(login)) + "|modulo=" + modulo;
                    _this.http.request(options.method, options.url, options)
                        .subscribe(function (data) {
                        var resData = data;
                        var res = JSON.parse(resData[0].value);
                        //let response = JSON.parse(data[0].value);
                        //loading.dismiss();
                        if (res.success == 'true') {
                            _this.userLogged = (JSON.parse(JSON.stringify(res.user[0])));
                            _this.storage.set('user', _this.userLogged);
                            resolve(res);
                        }
                        else {
                            reject(res.message);
                        }
                    }, function (error) {
                        console.log('Error on login', error);
                        //loading.dismiss();
                    });
                }
            }, function (error) {
                //loading.dismiss();
                if (error.name == 'TimeoutError') {
                    reject('Falha de conexão. Tente novamente mais tarde');
                }
                console.log('Error login', error);
            });
        });
    };
    /**
     *
     * @param modulo
     * @param msg
     * @param acao
     * @param params
     * @param map - indica se o conteudo retornado é um array json em formato string, que precisa de parsing em cada item
     * @param parse - indice que o objeto retornado é um json e deve ter um parse
     */
    ServidorProvider.prototype.getRequest = function (modulo, msg, acao, params, map, parse) {
        /*
        let loading = this.alertProvider.showLoading(msg);
        if (msg) {
          loading.present();
        }
        */
        if (map === void 0) { map = true; }
        if (parse === void 0) { parse = false; }
        var parametrosPadraoStr = "";
        if (this.parametrosPadrao) {
            parametrosPadraoStr = _.map(this.parametrosPadrao, function (value, key) { return key + "=" + value; }).join('|');
        }
        var options = {
            url: this.config.server['pathProcessar'],
            //headers: {'Content-Type' : 'text/plain;charset=ISO-8859-1'},
            params: {
                params: "modulo=" + modulo + "|acao=" + acao + "|params=" + JSON.stringify(params) + "|acesso={}|" + parametrosPadraoStr
            }
        };
        /** TODO
         * condição feita dessa forma para evitar bug nos pontos onde o parse está sendo feito no retorno da requisição
         * deve ser refeito para ficar na mesma condição onde está o "else if (map)"
         * código repetido
        **/
        if (parse) {
            return this.http.get(options.url, options)
                .map(function (data) {
                var value = {};
                var resData = data;
                try {
                    value = JSON.parse(resData[0].value);
                }
                catch (e) {
                    console.log('Tentativa de parse de objeto vazio');
                }
                /*
                if (msg)
                  loading.dismiss();
                  */
                return value;
            })
                .catch(function (err) {
                //loading.dismiss();
                console.error("Error request " + acao, err);
                return Observable.throw(err);
                //this.alertProvider.showAlert('', 'Desculpe ocorreu uma falha na requisição do serviço. Tente novamente mais tarde.', [{ text: 'OK' }]);
            });
        }
        else if (map) {
            return this.http.get(options.url, options)
                .map(function (data) {
                var resData = data;
                var errors = resData[0].errors;
                if (!errors) {
                    var value = JSON.parse(resData[0].value);
                    /*if (msg)
                      loading.dismiss();
                      */
                    if (isArray(value)) {
                        return value.map(function (item) { return JSON.parse(item); });
                    }
                }
                else {
                    throw new Error(errors['error']);
                }
            })
                .catch(function (e) {
                //loading.dismiss();
                console.error("Error request " + acao, e);
                //this.alertProvider.showAlert('', 'Desculpe ocorreu uma falha na requisição do serviço. Tente novamente mais tarde.', [{ text: 'OK' }]);
                return Observable.throw(e);
            });
        }
        else {
            if (msg)
                //loading.dismiss();
                return this.http
                    .get(options.url, options)
                    .catch(function (e) {
                    //loading.dismiss();
                    console.error("Error request " + acao, e);
                    //this.alertProvider.showAlert('', 'Desculpe, ocorreu uma falha na requisição do serviço. Tente novamente mais tarde.', [{ text: 'OK' }]);
                    return Observable.throw(e);
                });
        }
    };
    ServidorProvider.prototype.obterDadosPadrao = function (modulo, buscarServidor) {
        var _this = this;
        if (buscarServidor === void 0) { buscarServidor = true; }
        return new Promise(function (resolve, reject) {
            _this.storage.get("dados_padrao_" + modulo)
                .then(function (dados) {
                //console.log('dados padrao', dados);
                if (dados) {
                    dados = JSON.parse(dados);
                    resolve(dados);
                }
                else if (buscarServidor) {
                    _this.atualizarDadoPadrao(modulo).
                        then(function (dados) { return resolve(dados); });
                }
                else {
                    resolve();
                }
            })
                .catch(function (err) { return reject(err); });
        });
    };
    ServidorProvider.prototype.atualizarDadosPadrao = function (modulos) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var promises = _.map(modulos, function (modulo) { return _this.atualizarDadoPadrao(modulo); });
            Promise.all(promises)
                .then(function (result) { return resolve({ "success": true, "result": result }); })
                .catch(function (err) { return reject(err); });
        });
    };
    ServidorProvider.prototype.atualizarDadoPadrao = function (modulo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get("dados_padrao_" + modulo)
                .then(function (dados) {
                //console.log('atualizar dados padrão', dados);
                dados = JSON.parse(dados);
                var data;
                if (dados) {
                    data = dados.data;
                }
                _this.requestDadosPadrao(modulo, data)
                    .then(function (dadosParsed) {
                    console.log('#dadosParsed', dadosParsed);
                    if (dadosParsed.success == "true")
                        _this.storage.set("dados_padrao_" + modulo, JSON.stringify(dadosParsed));
                    resolve(dadosParsed);
                })
                    .catch(function (err) {
                    console.log("Houve um erro ao obter os dados padr\u00E3o: " + err);
                    reject(err);
                });
            });
        });
    };
    ServidorProvider.prototype.gravarDadoPadrao = function (modulo, dados) {
        console.log('gravarDadoPadrao #', modulo);
        return this.storage.set("dados_padrao_" + modulo, JSON.stringify(dados));
    };
    //implementar mecanismo de cache no servidor provider, p/ não acessar o storage sempre
    ServidorProvider.prototype.obterDadoPadrao = function (modulo, dado) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get("dados_padrao_" + modulo)
                .then(function (dados) {
                dados = JSON.parse(dados);
                console.log("ServidorProvider | ObterDadoPadrao", dados);
                resolve((dados) ? dados[dado] : null);
            })
                .catch(function (err) {
                console.log("Houve um erro ao obter dado padr\u00E3o [" + modulo + "][" + dado + "]");
                reject(err);
            });
        });
    };
    ServidorProvider.prototype.definirVariaveis = function (config) {
        var conf = new Config(); //{ 'server': {} };
        conf.server['urlBase'] = "http://" + config.url + ":" + config.port + "/";
        conf.server['callWebRulePath'] = 'synergy/CallWebRule';
        conf.server['loginPath'] = 'synergy/Login';
        conf.server['urlCallRule'] = conf.server['urlBase'] + conf.server['callWebRulePath'] +
            "?sys=" + config.sys +
            "&webrun=" + config.webrun +
            "&port=" + config.port +
            "&rule=";
        conf.server['urlOCR'] = "http://" + config.url + ":" + config.port_ocr + "/";
        conf.server['urlCallRuleSecondServer'] = conf.server['urlBase'] + conf.server['callWebRulePath'] +
            "?sys=" + config.sys_second +
            "&webrun=" + config.webrun +
            "&port=" + config.port +
            "&rule=";
        conf.server['pathProcessarSecondServer'] = conf.server['urlCallRuleSecondServer'] + config.regraProcessar;
        conf.server['pathProcessar'] = conf.server['urlCallRule'] + config.regraProcessar;
        // + "&params=acao=";
        /*
        conf.server.pathAutenticarPre = conf.server.acaoAutenticarPre //conf.server.pathProcessar;//;
            conf.server.pathAutenticar = conf.server.pathProcessar;// + conf.server.acaoAutenticar;
            conf.server.pathEnviarEstatistica = conf.server.pathProcessar + conf.server.acaoEstatistica;
            conf.server.pathConsultarPaciente = conf.server.pathProcessar + conf.server.acaoConsultarPaciente;
            conf.server.pathChecarPrescricaoObterDados = conf.server.pathProcessar + conf.server.acaoChecarPrescricaoObterDados;
            conf.server.pathChecarAprazamento = conf.server.pathProcessar + conf.server.acaoChecarAprazamento;
            */
        //conf.dataUltimaAtualizacao = that.getDataUltimaAtualizacao();
        return conf;
    };
    ServidorProvider.prototype.downloadPhoto = function (caminhoFoto) {
        var _this = this;
        var url = encodeURI(caminhoFoto), nameFile = new Date().getTime(), folder = (this.file.externalCacheDirectory || this.file.dataDirectory);
        return new Promise(function (resolve) {
            _this.fileTransfer.abort();
            _this.subject.next(0);
            _this.fileTransfer.onProgress(function (ev) {
                if (ev.lengthComputable)
                    _this.subject.next(ev.loaded / ev.total);
            });
            _this.fileTransfer
                .download(url, folder + nameFile + ".jpg")
                .then(function (data) {
                _this.file.readAsDataURL(folder, nameFile + ".jpg")
                    .then(function (imgBase64) {
                    resolve({ 'img': imgBase64, 'url': data.nativeURL });
                }).catch(function (e) { return console.error(JSON.stringify(e)); });
            }).catch(function (e) { return console.error(JSON.stringify(e)); });
        });
    };
    ServidorProvider.prototype.getProgressTransfer = function () {
        return this.subject;
    };
    ServidorProvider.prototype.uploadPhoto = function (foto, modulo, acao, params) {
        // let loading = this.alertProvider.showLoading('Enviando foto...');
        var _this = this;
        if (params === void 0) { params = {}; }
        return new Promise(function (resolve, reject) {
            if (foto) {
                var url = encodeURI(_this.config.server.pathProcessar + "&params=modulo=" + modulo + "|acao=" + acao + "|params=" + JSON.stringify(params) + "|acesso={}");
                //loading.present();
                console.log('envio de foto', foto, url, JSON.stringify(params));
                _this.fileTransfer.upload(foto, url)
                    .then(function (data) {
                    //loading.dismiss();
                    resolve();
                }, function (e) {
                    //loading.dismiss();
                    console.log('Error on send photo', JSON.stringify(e));
                    reject('Ocorreu um erro ao enviar foto');
                });
            }
            else
                resolve();
        });
    };
    ServidorProvider.prototype.requestDadosPadrao = function (modulo, data) {
        var _this = this;
        if (data === void 0) { data = "01/01/1899 00:00:00"; }
        var options = {
            url: this.config.server['pathProcessar'],
            params: {
                params: "modulo=" + modulo + "|acao=dados_padrao|params=" + JSON.stringify({ 'data': data }) + "|acesso={}"
            }
        };
        return new Promise(function (resolve, reject) {
            _this.http.get(options.url, options)
                .subscribe(function (data) {
                var resData = data;
                var aux = JSON.parse(resData[0].value), values = {};
                if (aux.success == 'true') {
                    Object.keys(aux).forEach(function (key) {
                        if (key != 'success' && key != 'msg' && key != 'data') {
                            var value = aux[key];
                            if (Array.isArray(value) && value[0] !== 'null' && typeof value[0] === 'string') {
                                values[key] = value.map(function (item) {
                                    return JSON.parse(item);
                                });
                            }
                            else {
                                values[key] = value;
                            }
                        }
                        else {
                            values[key] = aux[key];
                        }
                    });
                    resolve(values);
                }
                else {
                    resolve(aux);
                }
            });
        });
    };
    ServidorProvider.prototype.obterDadosDispositivo = function (login) {
        if (!this.platform.is('android') || !this.platform.is('ios') || !this.platform.is('cordova')) {
            var dados = {
                "plataforma": navigator.platform,
                "usuario": login
            };
            return dados;
        }
        else {
            var dados = {
                "plataforma": this.device.platform,
                "versao_so": this.device.version,
                "aparelho": this.device.model,
                "versao_cordova": this.device.cordova,
                "uuid": this.device.uuid,
                "tipo_conexao": this.network.type,
                "usuario": login
            };
            /* dados["ip"] = this.ipAddress; */
            return dados;
        }
    };
    /**
     * Definir variável que guarda os parâmetros que toda requisição efetuada pelo método getRequest() irá mandar por padrão
     * @param parametros
     */
    ServidorProvider.prototype.definirParametrosPadrao = function (parametros) {
        this.parametrosPadrao = parametros;
    };
    ServidorProvider.prototype.getConfig = function (init) {
        if (init) {
            var config = window.localStorage.getItem(ServidorProvider.CONFIG_NAME);
            config = (config) ? config : '';
            this.config = this.definirVariaveis(JSON.parse(config));
        }
        return this.config;
    };
    ServidorProvider.CONFIG_NAME = 'servocliente_config'; // TODO receber na config do modulo
    ServidorProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    ServidorProvider.ctorParameters = function () { return [
        { type: File },
        { type: Platform },
        { type: Network },
        { type: FileTransfer },
        { type: Device },
        { type: HttpClient },
        { type: Storage }
    ]; };
    return ServidorProvider;
}());
export { ServidorProvider };
//# sourceMappingURL=servidor-provider.js.map
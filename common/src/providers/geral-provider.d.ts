import { Storage } from '@ionic/storage';
import { User } from '../models/user.model';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular/platform/platform';
import { MenuController, Events } from 'ionic-angular';
export declare class GeralProvider {
    private events;
    private menuCtrl;
    private storage;
    private camera;
    private platform;
    private file;
    globalVar: any;
    constructor(events: Events, menuCtrl: MenuController, storage: Storage, camera: Camera, platform: Platform, file: File);
    removeCache(): void;
    initMenu(user: User, pages: any[]): void;
    dateISOToLocalFormat(dateISO: string, time?: boolean): string;
    dateLocalFormatToISO(dateLocalFormat: string, time?: boolean): string;
    numeroAleatorio(mínimo?: number, máximo?: number): number;
    calcularIdade(dataAniversario: Date): number;
    _workaroundFormControlApplyMask(element: any): void;
}

import { NgModule } from '@angular/core';
import { CommonModule as AngularCommonModule } from '@angular/common';
import { ServidorProvider } from './providers/servidor-provider';
import { CommonModule } from '../../common/src/common.module';
import { ServidorConfigService } from './providers/servidor-config-service';
var WebrunModule = /** @class */ (function () {
    function WebrunModule() {
    }
    WebrunModule.forRoot = function (config) {
        return {
            ngModule: WebrunModule,
            providers: [
                ServidorProvider,
                {
                    provide: ServidorConfigService,
                    useValue: config
                }
            ]
        };
    };
    WebrunModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [],
                    imports: [AngularCommonModule, CommonModule],
                    exports: [],
                    providers: [ServidorProvider],
                },] },
    ];
    return WebrunModule;
}());
export { WebrunModule };
//# sourceMappingURL=webrun.module.js.map
import { NgModule } from '@angular/core';
import { CommonModule as AngularCommonModule } from '@angular/common';
import { ServidorProvider } from './providers/servidor-provider';
import { CommonModule } from 'common/src/common.module';
var WebrunModule = /** @class */ (function () {
    function WebrunModule() {
    }
    WebrunModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [],
                    imports: [AngularCommonModule, CommonModule],
                    exports: [],
                    providers: [ServidorProvider],
                },] },
    ];
    return WebrunModule;
}());
export { WebrunModule };
//# sourceMappingURL=webrun.module.js.map
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { App } from 'ionic-angular/components/app/app';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { MenuController } from 'ionic-angular';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Alert } from 'ionic-angular/components/alert/alert';
import { Loading } from 'ionic-angular/components/loading/loading';
export declare class AlertProvider {
    private storage;
    private menuCtrl;
    private loadingCtrl;
    private toastCtrl;
    private alertCtrl;
    protected app: App;
    constructor(storage: Storage, menuCtrl: MenuController, loadingCtrl: LoadingController, toastCtrl: ToastController, alertCtrl: AlertController, app: App);
    showToast(message: string, duration?: number): void;
    showAlert(title?: string, message?: string, btns?: Array<any>, timeout?: number, inputs?: Array<any>): Alert;
    showLoading(message?: string): Loading;
    showAlertExit(paginaRoot: string): void;
    readonly navCtrl: NavController;
}

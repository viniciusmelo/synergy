export default class LoginResponseData {
    success: string;
    pass: string;
    message: string;
    user: string;
}

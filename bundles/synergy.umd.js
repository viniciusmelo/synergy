(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs/BehaviorSubject'), require('ionic-angular'), require('@ionic-native/device'), require('@ionic-native/network'), require('ts-md5'), require('@angular/common/http'), require('rxjs/add/operator/toPromise'), require('rxjs/add/operator/timeout'), require('rxjs/add/operator/catch'), require('rxjs/add/operator/map'), require('rxjs/add/observable/throw'), require('rxjs/Observable'), require('ionic-angular/util/util'), require('@ionic/storage'), require('@ionic-native/file-transfer'), require('@ionic-native/file'), require('lodash'), require('@ionic-native/camera'), require('ionic-angular/platform/platform'), require('ionic-angular/components/alert/alert-controller'), require('ionic-angular/components/app/app'), require('ionic-angular/components/loading/loading-controller'), require('ionic-angular/components/toast/toast-controller'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define(['exports', '@angular/core', 'rxjs/BehaviorSubject', 'ionic-angular', '@ionic-native/device', '@ionic-native/network', 'ts-md5', '@angular/common/http', 'rxjs/add/operator/toPromise', 'rxjs/add/operator/timeout', 'rxjs/add/operator/catch', 'rxjs/add/operator/map', 'rxjs/add/observable/throw', 'rxjs/Observable', 'ionic-angular/util/util', '@ionic/storage', '@ionic-native/file-transfer', '@ionic-native/file', 'lodash', '@ionic-native/camera', 'ionic-angular/platform/platform', 'ionic-angular/components/alert/alert-controller', 'ionic-angular/components/app/app', 'ionic-angular/components/loading/loading-controller', 'ionic-angular/components/toast/toast-controller', '@angular/common'], factory) :
    (factory((global.ng = global.ng || {}, global.ng.synergy = {}),global.ng.core,global.BehaviorSubject,global.ionicAngular,global.device,global.network,global.tsMd5,global.http,null,null,null,global.Rx.Observable.prototype,null,global.Rx,global.util,global.storage,global.fileTransfer,global.file,global._,global.camera,global.platform,global.alertController,global.app,global.loadingController,global.toastController,global.common));
}(this, (function (exports,core,BehaviorSubject,ionicAngular,device,network,tsMd5,http,toPromise,timeout,_catch,map,_throw,Observable,util,storage,fileTransfer,file,_,camera,platform,alertController,app,loadingController,toastController,common) { 'use strict';

    var Config = /** @class */ (function () {
        function Config() {
        }
        return Config;
    }());

    var ServidorConfigService = new core.InjectionToken("ServidorConfig");

    //import { User } from '../models/user.model';
    var ServidorProvider = /** @class */ (function () {
        function ServidorProvider(configService, file$$1, platform$$1, network$$1, transfer, device$$1, http$$1, storage$$1) {
            var _this = this;
            this.configService = configService;
            this.file = file$$1;
            this.platform = platform$$1;
            this.network = network$$1;
            this.transfer = transfer;
            this.device = device$$1;
            this.http = http$$1;
            this.storage = storage$$1;
            this.fileTransfer = this.transfer.create();
            this.subject = new BehaviorSubject.BehaviorSubject(0);
            this.init();
            if (!this.userLogged) {
                this.storage.get('user')
                    .then(function (data) {
                    if (data) {
                        _this.userLogged = JSON.parse(data);
                    }
                });
            }
        }
        ServidorProvider.prototype.getConfigName = function () {
            return this.configService.persistenceName;
        };
        Object.defineProperty(ServidorProvider.prototype, "user", {
            get: function () {
                return this.userLogged;
            },
            enumerable: true,
            configurable: true
        });
        ServidorProvider.prototype.init = function () {
            this.getConfig(true);
        };
        ServidorProvider.prototype.login = function (login, senha, modulo) {
            var _this = this;
            //const loading = this.alertProvider.showLoading('Carregando...');
            //loading.present();
            this.init();
            var params = {
                "user": login + ""
            }, options = {
                method: "get",
                url: this.config.server['pathProcessar'],
                params: {
                    params: "acao=pre_login|params=" + JSON.stringify(params) + "|acesso=" + JSON.stringify(this.obterDadosDispositivo(login)) + "|modulo=" + modulo
                }
            };
            return new Promise(function (resolve, reject) {
                //Pre login
                _this.http.request("GET", options.url, options)
                    .timeout(120000)
                    .toPromise()
                    .then(function (data) {
                    var resData = data;
                    var res = JSON.parse(resData[0].value);
                    if (res.success == 'false') {
                        //loading.dismiss();
                        reject(res.message);
                    }
                    if (res.success == 'true') {
                        var loginParams = {
                            "pass": tsMd5.Md5.hashStr(res.user + senha),
                            "user": params.user
                        };
                        //Login 
                        options.params.params = "acao=login|params=" + JSON.stringify(loginParams) + "|acesso=" + JSON.stringify(_this.obterDadosDispositivo(login)) + "|modulo=" + modulo;
                        _this.http.request(options.method, options.url, options)
                            .subscribe(function (data) {
                            var resData = data;
                            var res = JSON.parse(resData[0].value);
                            //let response = JSON.parse(data[0].value);
                            //loading.dismiss();
                            if (res.success == 'true') {
                                _this.userLogged = (JSON.parse(JSON.stringify(res.user[0])));
                                _this.storage.set('user', _this.userLogged);
                                resolve(res);
                            }
                            else {
                                reject(res.message);
                            }
                        }, function (error) {
                            console.log('Error on login', error);
                            //loading.dismiss();
                        });
                    }
                }, function (error) {
                    //loading.dismiss();
                    if (error.name == 'TimeoutError') {
                        reject('Falha de conexão. Tente novamente mais tarde');
                    }
                    console.log('Error login', error);
                });
            });
        };
        /**
         *
         * @param modulo
         * @param msg
         * @param acao
         * @param params
         * @param map - indica se o conteudo retornado é um array json em formato string, que precisa de parsing em cada item
         * @param parse - indice que o objeto retornado é um json e deve ter um parse
         */
        ServidorProvider.prototype.getRequest = function (modulo, msg, acao, params, map$$1, parse) {
            /*
            let loading = this.alertProvider.showLoading(msg);
            if (msg) {
              loading.present();
            }
            */
            if (map$$1 === void 0) { map$$1 = true; }
            if (parse === void 0) { parse = false; }
            var parametrosPadraoStr = "";
            if (this.parametrosPadrao) {
                parametrosPadraoStr = _.map(this.parametrosPadrao, function (value, key) { return key + "=" + value; }).join('|');
            }
            var options = {
                url: this.config.server['pathProcessar'],
                //headers: {'Content-Type' : 'text/plain;charset=ISO-8859-1'},
                params: {
                    params: "modulo=" + modulo + "|acao=" + acao + "|params=" + JSON.stringify(params) + "|acesso={}|" + parametrosPadraoStr
                }
            };
            /** TODO
             * condição feita dessa forma para evitar bug nos pontos onde o parse está sendo feito no retorno da requisição
             * deve ser refeito para ficar na mesma condição onde está o "else if (map)"
             * código repetido
            **/
            if (parse) {
                return this.http.get(options.url, options)
                    .map(function (data) {
                    var value = {};
                    var resData = data;
                    try {
                        value = JSON.parse(resData[0].value);
                    }
                    catch (e) {
                        console.log('Tentativa de parse de objeto vazio');
                    }
                    /*
                    if (msg)
                      loading.dismiss();
                      */
                    return value;
                })
                    .catch(function (err) {
                    //loading.dismiss();
                    console.error("Error request " + acao, err);
                    return Observable.Observable.throw(err);
                    //this.alertProvider.showAlert('', 'Desculpe ocorreu uma falha na requisição do serviço. Tente novamente mais tarde.', [{ text: 'OK' }]);
                });
            }
            else if (map$$1) {
                return this.http.get(options.url, options)
                    .map(function (data) {
                    var resData = data;
                    var errors = resData[0].errors;
                    if (!errors) {
                        var value = JSON.parse(resData[0].value);
                        /*if (msg)
                          loading.dismiss();
                          */
                        if (util.isArray(value)) {
                            return value.map(function (item) { return JSON.parse(item); });
                        }
                    }
                    else {
                        throw new Error(errors['error']);
                    }
                })
                    .catch(function (e) {
                    //loading.dismiss();
                    console.error("Error request " + acao, e);
                    //this.alertProvider.showAlert('', 'Desculpe ocorreu uma falha na requisição do serviço. Tente novamente mais tarde.', [{ text: 'OK' }]);
                    return Observable.Observable.throw(e);
                });
            }
            else {
                if (msg)
                    //loading.dismiss();
                    return this.http
                        .get(options.url, options)
                        .catch(function (e) {
                        //loading.dismiss();
                        console.error("Error request " + acao, e);
                        //this.alertProvider.showAlert('', 'Desculpe, ocorreu uma falha na requisição do serviço. Tente novamente mais tarde.', [{ text: 'OK' }]);
                        return Observable.Observable.throw(e);
                    });
            }
        };
        ServidorProvider.prototype.obterDadosPadrao = function (modulo, buscarServidor) {
            var _this = this;
            if (buscarServidor === void 0) { buscarServidor = true; }
            return new Promise(function (resolve, reject) {
                _this.storage.get("dados_padrao_" + modulo)
                    .then(function (dados) {
                    //console.log('dados padrao', dados);
                    if (dados) {
                        dados = JSON.parse(dados);
                        resolve(dados);
                    }
                    else if (buscarServidor) {
                        _this.atualizarDadoPadrao(modulo).
                            then(function (dados) { return resolve(dados); });
                    }
                    else {
                        resolve();
                    }
                })
                    .catch(function (err) { return reject(err); });
            });
        };
        ServidorProvider.prototype.atualizarDadosPadrao = function (modulos) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var promises = _.map(modulos, function (modulo) { return _this.atualizarDadoPadrao(modulo); });
                Promise.all(promises)
                    .then(function (result) { return resolve({ "success": true, "result": result }); })
                    .catch(function (err) { return reject(err); });
            });
        };
        ServidorProvider.prototype.atualizarDadoPadrao = function (modulo) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.storage.get("dados_padrao_" + modulo)
                    .then(function (dados) {
                    //console.log('atualizar dados padrão', dados);
                    dados = JSON.parse(dados);
                    var data;
                    if (dados) {
                        data = dados.data;
                    }
                    _this.requestDadosPadrao(modulo, data)
                        .then(function (dadosParsed) {
                        console.log('#dadosParsed', dadosParsed);
                        if (dadosParsed.success == "true")
                            _this.storage.set("dados_padrao_" + modulo, JSON.stringify(dadosParsed));
                        resolve(dadosParsed);
                    })
                        .catch(function (err) {
                        console.log("Houve um erro ao obter os dados padr\u00E3o: " + err);
                        reject(err);
                    });
                });
            });
        };
        ServidorProvider.prototype.gravarDadoPadrao = function (modulo, dados) {
            console.log('gravarDadoPadrao #', modulo);
            return this.storage.set("dados_padrao_" + modulo, JSON.stringify(dados));
        };
        //implementar mecanismo de cache no servidor provider, p/ não acessar o storage sempre
        ServidorProvider.prototype.obterDadoPadrao = function (modulo, dado) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.storage.get("dados_padrao_" + modulo)
                    .then(function (dados) {
                    dados = JSON.parse(dados);
                    console.log("ServidorProvider | ObterDadoPadrao", dados);
                    resolve((dados) ? dados[dado] : null);
                })
                    .catch(function (err) {
                    console.log("Houve um erro ao obter dado padr\u00E3o [" + modulo + "][" + dado + "]");
                    reject(err);
                });
            });
        };
        ServidorProvider.prototype.definirVariaveis = function (config) {
            var conf = new Config(); //{ 'server': {} };
            conf.server['urlBase'] = "http://" + config.url + ":" + config.port + "/";
            conf.server['callWebRulePath'] = this.configService.synergyContext + "/CallWebRule";
            conf.server['loginPath'] = this.configService.synergyContext + "/Login";
            conf.server['urlCallRule'] = conf.server['urlBase'] + conf.server['callWebRulePath'] +
                "?sys=" + config.sys +
                "&webrun=" + config.webrun +
                "&port=" + config.port +
                "&rule=";
            conf.server['urlOCR'] = "http://" + config.url + ":" + config.port_ocr + "/";
            conf.server['urlCallRuleSecondServer'] = conf.server['urlBase'] + conf.server['callWebRulePath'] +
                "?sys=" + config.sys_second +
                "&webrun=" + config.webrun +
                "&port=" + config.port +
                "&rule=";
            conf.server['pathProcessarSecondServer'] = conf.server['urlCallRuleSecondServer'] + config.regraProcessar;
            conf.server['pathProcessar'] = conf.server['urlCallRule'] + config.regraProcessar;
            // + "&params=acao=";
            /*
            conf.server.pathAutenticarPre = conf.server.acaoAutenticarPre //conf.server.pathProcessar;//;
                conf.server.pathAutenticar = conf.server.pathProcessar;// + conf.server.acaoAutenticar;
                conf.server.pathEnviarEstatistica = conf.server.pathProcessar + conf.server.acaoEstatistica;
                conf.server.pathConsultarPaciente = conf.server.pathProcessar + conf.server.acaoConsultarPaciente;
                conf.server.pathChecarPrescricaoObterDados = conf.server.pathProcessar + conf.server.acaoChecarPrescricaoObterDados;
                conf.server.pathChecarAprazamento = conf.server.pathProcessar + conf.server.acaoChecarAprazamento;
                */
            //conf.dataUltimaAtualizacao = that.getDataUltimaAtualizacao();
            return conf;
        };
        ServidorProvider.prototype.downloadPhoto = function (caminhoFoto) {
            var _this = this;
            var url = encodeURI(caminhoFoto), nameFile = new Date().getTime(), folder = (this.file.externalCacheDirectory || this.file.dataDirectory);
            return new Promise(function (resolve) {
                _this.fileTransfer.abort();
                _this.subject.next(0);
                _this.fileTransfer.onProgress(function (ev) {
                    if (ev.lengthComputable)
                        _this.subject.next(ev.loaded / ev.total);
                });
                _this.fileTransfer
                    .download(url, folder + nameFile + ".jpg")
                    .then(function (data) {
                    _this.file.readAsDataURL(folder, nameFile + ".jpg")
                        .then(function (imgBase64) {
                        resolve({ 'img': imgBase64, 'url': data.nativeURL });
                    }).catch(function (e) { return console.error(JSON.stringify(e)); });
                }).catch(function (e) { return console.error(JSON.stringify(e)); });
            });
        };
        ServidorProvider.prototype.getProgressTransfer = function () {
            return this.subject;
        };
        ServidorProvider.prototype.uploadPhoto = function (foto, modulo, acao, params) {
            // let loading = this.alertProvider.showLoading('Enviando foto...');
            var _this = this;
            if (params === void 0) { params = {}; }
            return new Promise(function (resolve, reject) {
                if (foto) {
                    var url = encodeURI(_this.config.server.pathProcessar + "&params=modulo=" + modulo + "|acao=" + acao + "|params=" + JSON.stringify(params) + "|acesso={}");
                    //loading.present();
                    console.log('envio de foto', foto, url, JSON.stringify(params));
                    _this.fileTransfer.upload(foto, url)
                        .then(function (data) {
                        //loading.dismiss();
                        resolve();
                    }, function (e) {
                        //loading.dismiss();
                        console.log('Error on send photo', JSON.stringify(e));
                        reject('Ocorreu um erro ao enviar foto');
                    });
                }
                else
                    resolve();
            });
        };
        ServidorProvider.prototype.requestDadosPadrao = function (modulo, data) {
            var _this = this;
            if (data === void 0) { data = "01/01/1899 00:00:00"; }
            var options = {
                url: this.config.server['pathProcessar'],
                params: {
                    params: "modulo=" + modulo + "|acao=dados_padrao|params=" + JSON.stringify({ 'data': data }) + "|acesso={}"
                }
            };
            return new Promise(function (resolve, reject) {
                _this.http.get(options.url, options)
                    .subscribe(function (data) {
                    var resData = data;
                    var aux = JSON.parse(resData[0].value), values = {};
                    if (aux.success == 'true') {
                        Object.keys(aux).forEach(function (key) {
                            if (key != 'success' && key != 'msg' && key != 'data') {
                                var value = aux[key];
                                if (Array.isArray(value) && value[0] !== 'null' && typeof value[0] === 'string') {
                                    values[key] = value.map(function (item) {
                                        return JSON.parse(item);
                                    });
                                }
                                else {
                                    values[key] = value;
                                }
                            }
                            else {
                                values[key] = aux[key];
                            }
                        });
                        resolve(values);
                    }
                    else {
                        resolve(aux);
                    }
                });
            });
        };
        ServidorProvider.prototype.obterDadosDispositivo = function (login) {
            if (!this.platform.is('android') || !this.platform.is('ios') || !this.platform.is('cordova')) {
                var dados = {
                    "plataforma": navigator.platform,
                    "usuario": login
                };
                return dados;
            }
            else {
                var dados = {
                    "plataforma": this.device.platform,
                    "versao_so": this.device.version,
                    "aparelho": this.device.model,
                    "versao_cordova": this.device.cordova,
                    "uuid": this.device.uuid,
                    "tipo_conexao": this.network.type,
                    "usuario": login
                };
                /* dados["ip"] = this.ipAddress; */
                return dados;
            }
        };
        /**
         * Definir variável que guarda os parâmetros que toda requisição efetuada pelo método getRequest() irá mandar por padrão
         * @param parametros
         */
        ServidorProvider.prototype.definirParametrosPadrao = function (parametros) {
            this.parametrosPadrao = parametros;
        };
        ServidorProvider.prototype.getConfig = function (init) {
            if (init) {
                var config = window.localStorage.getItem(this.configService.persistenceName);
                config = (config) ? config : '';
                this.config = this.definirVariaveis(JSON.parse(config));
            }
            return this.config;
        };
        ServidorProvider.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        ServidorProvider.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [ServidorConfigService,] }] },
            { type: file.File },
            { type: ionicAngular.Platform },
            { type: network.Network },
            { type: fileTransfer.FileTransfer },
            { type: device.Device },
            { type: http.HttpClient },
            { type: storage.Storage }
        ]; };
        return ServidorProvider;
    }());

    var GeralProvider = /** @class */ (function () {
        function GeralProvider(events, menuCtrl, storage$$1, camera$$1, platform$$1, file$$1) {
            this.events = events;
            this.menuCtrl = menuCtrl;
            this.storage = storage$$1;
            this.camera = camera$$1;
            this.platform = platform$$1;
            this.file = file$$1;
            this.globalVar = {};
        }
        GeralProvider.prototype.removeCache = function () {
            var _this = this;
            if (this.platform.is('android')) {
                this.file.listDir(this.file.externalCacheDirectory, '').then(function (fotos) {
                    if (fotos) {
                        if (fotos.length > 5) {
                            fotos.forEach(function (item) {
                                _this.file.removeFile(_this.file.externalCacheDirectory, item.name)
                                    .then()
                                    .catch(function (e) { return console.log(e); });
                                console.log('Photos removed from cache');
                            });
                        }
                    }
                }).catch(function (e) { return console.log(e); });
            }
            else {
                this.camera.cleanup()
                    .then(function () { return console.log('Photos removed from cache'); })
                    .catch(function (e) { return console.error(e); });
            }
        };
        GeralProvider.prototype.initMenu = function (user, pages) {
            var _this = this;
            this.menuCtrl.enable(true);
            var menu = { 'pages': pages };
            if (user) {
                this.storage.set('user', JSON.stringify(user));
                menu['user'] = user;
                this.events.publish('menu:populate', menu);
            }
            else {
                this.storage.get('user').then(function (data) {
                    user = JSON.parse(data);
                    menu['user'] = user;
                    _this.events.publish('menu:populate', menu);
                });
            }
        };
        GeralProvider.prototype.dateISOToLocalFormat = function (dateISO, time) {
            if (time === void 0) { time = false; }
            var fullDateParts = dateISO.split('T');
            var dateParts = fullDateParts[0].split('-');
            var timeParts = fullDateParts[1].split(':');
            var dateResult = dateParts[2] + "/" + dateParts[1] + "/" + dateParts[0];
            dateResult += (time) ? " " + timeParts[0] + ":" + timeParts[1] : '';
            return dateResult;
        };
        GeralProvider.prototype.dateLocalFormatToISO = function (dateLocalFormat, time) {
            if (time === void 0) { time = false; }
            var myDate = new Date(dateLocalFormat.substr(3, 2) + "/" + dateLocalFormat.substr(0, 2) + "/" + dateLocalFormat.substr(6, 4));
            if (time) {
                var fullDateParts = dateLocalFormat.split(' ');
                var timeParts = fullDateParts[1].split(':');
                myDate.setHours(+timeParts[0]);
                myDate.setMinutes(+timeParts[1]);
            }
            return myDate.toISOString();
        };
        GeralProvider.prototype.numeroAleatorio = function (mínimo, máximo) {
            if (mínimo === void 0) { mínimo = 0; }
            if (máximo === void 0) { máximo = 99999999; }
            return Math.round(Math.random() * (máximo - mínimo) + mínimo);
        };
        GeralProvider.prototype.calcularIdade = function (dataAniversario) {
            var ageDifMs = Date.now() - dataAniversario.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        };
        GeralProvider.prototype._workaroundFormControlApplyMask = function (element) {
            element.nativeElement.firstElementChild.dispatchEvent(new Event('focus'));
            element.nativeElement.firstElementChild.dispatchEvent(new Event('blur'));
            element.nativeElement.firstElementChild.dispatchEvent(new Event('input'));
        };
        GeralProvider.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        GeralProvider.ctorParameters = function () { return [
            { type: ionicAngular.Events },
            { type: ionicAngular.MenuController },
            { type: storage.Storage },
            { type: camera.Camera },
            { type: platform.Platform },
            { type: file.File }
        ]; };
        return GeralProvider;
    }());

    var AlertProvider = /** @class */ (function () {
        function AlertProvider(storage$$1, menuCtrl, loadingCtrl, toastCtrl, alertCtrl, app$$1) {
            this.storage = storage$$1;
            this.menuCtrl = menuCtrl;
            this.loadingCtrl = loadingCtrl;
            this.toastCtrl = toastCtrl;
            this.alertCtrl = alertCtrl;
            this.app = app$$1;
        }
        AlertProvider.prototype.showToast = function (message, duration) {
            if (duration === void 0) { duration = 2000; }
            this.toastCtrl.create({ message: message, duration: duration }).present();
        };
        AlertProvider.prototype.showAlert = function (title, message, btns, timeout$$1, inputs) {
            var alert = this.alertCtrl.create({
                inputs: inputs,
                title: title,
                message: message,
                buttons: btns
            });
            alert.present();
            if (timeout$$1) {
                setTimeout(function () {
                    alert.dismiss();
                }, timeout$$1);
            }
            return alert;
        };
        AlertProvider.prototype.showLoading = function (message) {
            return this.loadingCtrl.create({ content: message });
        };
        AlertProvider.prototype.showAlertExit = function (paginaRoot) {
            var _this = this;
            var alerta = this.alertCtrl.create({
                subTitle: 'Você tem certeza que deseja sair?',
                buttons: [
                    {
                        text: 'Não',
                        handler: function () { }
                    },
                    {
                        text: 'Sim',
                        handler: function () {
                            _this.navCtrl.setRoot(paginaRoot);
                            _this.menuCtrl.enable(false);
                            _this.storage.remove('user');
                        }
                    }
                ]
            });
            alerta.present();
        };
        Object.defineProperty(AlertProvider.prototype, "navCtrl", {
            get: function () {
                return this.app.getRootNav();
            },
            enumerable: true,
            configurable: true
        });
        AlertProvider.decorators = [
            { type: core.Injectable },
        ];
        /** @nocollapse */
        AlertProvider.ctorParameters = function () { return [
            { type: storage.Storage },
            { type: ionicAngular.MenuController },
            { type: loadingController.LoadingController },
            { type: toastController.ToastController },
            { type: alertController.AlertController },
            { type: app.App }
        ]; };
        return AlertProvider;
    }());

    var CommonModule = /** @class */ (function () {
        function CommonModule() {
        }
        CommonModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [],
                        imports: [common.CommonModule],
                        exports: [],
                        providers: [GeralProvider, AlertProvider],
                    },] },
        ];
        return CommonModule;
    }());

    var WebrunModule = /** @class */ (function () {
        function WebrunModule() {
        }
        WebrunModule.forRoot = function (config) {
            return {
                ngModule: WebrunModule,
                providers: [
                    ServidorProvider,
                    {
                        provide: ServidorConfigService,
                        useValue: config
                    }
                ]
            };
        };
        WebrunModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [],
                        imports: [common.CommonModule, CommonModule],
                        exports: [],
                        providers: [ServidorProvider],
                    },] },
        ];
        return WebrunModule;
    }());

    exports.WebrunModule = WebrunModule;
    exports.CommonModule = CommonModule;
    exports.ServidorProvider = ServidorProvider;
    exports.AlertProvider = AlertProvider;
    exports.GeralProvider = GeralProvider;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

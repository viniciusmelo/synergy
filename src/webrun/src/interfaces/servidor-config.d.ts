export interface ServidorConfig {
    persistenceName: string;
    synergyContext: string;
}

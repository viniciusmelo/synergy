export default class ResponseData {
    value: string;
    success: string;
    errors?: {
        error: string;
    };
}

/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
import * as i0 from "@angular/core";
import * as i1 from "./common.module";
import * as i2 from "@angular/common";
import * as i3 from "./providers/geral-provider";
import * as i4 from "ionic-angular/util/events";
import * as i5 from "ionic-angular/components/app/menu-controller";
import * as i6 from "@ionic/storage/dist/storage";
import * as i7 from "@ionic-native/camera";
import * as i8 from "ionic-angular/platform/platform";
import * as i9 from "@ionic-native/file";
import * as i10 from "./providers/alert-provider";
import * as i11 from "ionic-angular/components/loading/loading-controller";
import * as i12 from "ionic-angular/components/toast/toast-controller";
import * as i13 from "ionic-angular/components/alert/alert-controller";
import * as i14 from "ionic-angular/components/app/app";
var CommonModuleNgFactory = i0.ɵcmf(i1.CommonModule, [], function (_l) { return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, []], [3, i0.ComponentFactoryResolver], i0.NgModuleRef]), i0.ɵmpd(4608, i2.NgLocalization, i2.NgLocaleLocalization, [i0.LOCALE_ID, [2, i2.ɵa]]), i0.ɵmpd(4608, i3.GeralProvider, i3.GeralProvider, [i4.Events, i5.MenuController, i6.Storage, i7.Camera, i8.Platform, i9.File]), i0.ɵmpd(4608, i10.AlertProvider, i10.AlertProvider, [i6.Storage, i5.MenuController, i11.LoadingController, i12.ToastController, i13.AlertController, i14.App]), i0.ɵmpd(1073742336, i2.CommonModule, i2.CommonModule, []), i0.ɵmpd(1073742336, i1.CommonModule, i1.CommonModule, [])]); });
export { CommonModuleNgFactory as CommonModuleNgFactory };
//# sourceMappingURL=common.module.ngfactory.js.map
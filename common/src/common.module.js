import { NgModule } from '@angular/core';
import { CommonModule as AngularCommonModule } from '@angular/common';
import { GeralProvider } from './providers/geral-provider';
import { AlertProvider } from './providers/alert-provider';
var CommonModule = /** @class */ (function () {
    function CommonModule() {
    }
    CommonModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [],
                    imports: [AngularCommonModule],
                    exports: [],
                    providers: [GeralProvider, AlertProvider],
                },] },
    ];
    return CommonModule;
}());
export { CommonModule };
//# sourceMappingURL=common.module.js.map
export default class PreLoginResponseData {
    success: string;
    pass: string;
    message: string;
    user: string;
}
